package admin.lokacart.ict.mobile.com.adminapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import admin.lokacart.ict.mobile.com.adminapp.interfaces.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.container.SavedOrder;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class OrderRecyclerViewAdapter extends RecyclerView.Adapter<OrderRecyclerViewAdapter.DataObjectHolder>
{
    private final Context context;
    private List<SavedOrder> orderList = new ArrayList<>();
    private final Boolean paymentStatus;
    private final String tag;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        final TextView orderid,deldate;
        final TextView username;
        final TextView date;
        final TextView payment;
        // --Commented out by Inspection (28/11/16 11:42 AM):TextView deldate;
        final MyListener callback;
        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            orderid = (TextView) itemView.findViewById(R.id.order_Id);
            username = (TextView) itemView.findViewById(R.id.username);
            date = (TextView) itemView.findViewById(R.id.date);
            payment=(TextView) itemView.findViewById(R.id.paymentStatus);
            deldate = (TextView) itemView.findViewById(R.id.delDate);
            callback = (MyListener) context;
        }
    }

    public OrderRecyclerViewAdapter(ArrayList<SavedOrder> myDataset, Context context, String tag, Boolean paymentStatus)
    {
        orderList = myDataset;
        this.context = context;
        this.paymentStatus=paymentStatus;
        this.tag = tag;
    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.orders_list_item_card_view, parent, false);
        return new DataObjectHolder(view, context);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        SavedOrder order = orderList.get(position);
        holder.orderid.setText("" + order.getOrderId());
        holder.username.setText(order.getUserName());

        if(paymentStatus){
            holder.payment.setTextColor(Color.GREEN);
            if(order.getPayment().equals("true")){
                holder.payment.setText(R.string.textview_paid);
            }
            else
            {
                holder.payment.setText(R.string.textview_unpaid);
                holder.payment.setTextColor(Color.RED);
            }

        }else{
            holder.payment.setVisibility(View.GONE);
        }

        Date time = null;
        try {
            time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(order.getTimeStamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("EEE, MMM d, yyyy").format(time);
        holder.date.setText("Order Date: "+date);

         String deld = order.getDelDate();


        switch (deld)
        {
            case "not found":
                holder.deldate.setText("Delivery Date: Not Applicable");
                break;
            case "saved":
                break;
            case "cancelled":
                break;
            default:
                try {

                    if(!deld.equals("Not Applicable"))
                    {
                        Date delTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(deld);
                        deld = new SimpleDateFormat("EEE, MMM d, yyyy").format(delTime);
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                holder.deldate.setText("Delivery Date: " + deld);
                break;
        }
        if(tag == "PlacedOrderFragment" || tag == "CancelledOrderFragment"){
            holder.date.setGravity(Gravity.CENTER);
            holder.deldate.setVisibility(View.GONE);
        }




    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

}