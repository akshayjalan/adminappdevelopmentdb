package admin.lokacart.ict.mobile.com.adminapp.fragment;

/**
 * Created by root on 18/1/16.
 */
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.wx.wheelview.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import admin.lokacart.ict.mobile.com.adminapp.adapter.WheelViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.activity.EditOrderActivity;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.adapter.OrderDetailsRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.adapter.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.adapter.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.container.SavedOrder;

public class PlacedOrderFragment extends Fragment {


    private View savedOrderFragmentView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private JSONObject responseObject;
    private static int recyclerViewIndex;
    private SwipeRefreshLayout swipeContainer;
    private int count = 0;
    private TextView tOrders;
    private String stockEnabledStatus = "false";
    private String minimumBillAmount;
    private Context contextDialog;
    private WheelView wheelView;
    public static final String TAG = "PlacedOrderFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        savedOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);

        return savedOrderFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Master.savedOrderArrayList = new ArrayList<>();
    }


    private ArrayList<SavedOrder> getOrders(JSONArray jsonArray) {

        ArrayList<SavedOrder> orders = new ArrayList<>();
        int size= jsonArray.length();


        if(size!=0)

        {

            for(recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
            {

                try {

                    SavedOrder order = new SavedOrder((JSONObject) jsonArray.get(recyclerViewIndex), recyclerViewIndex, Master.PLACEDORDER);
                    orders.add(order);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }

        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
            new GetSavedOrderDetails(true).execute();
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();



        mRecyclerView = (RecyclerView) savedOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        swipeContainer = (SwipeRefreshLayout) savedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Master.isNetworkAvailable(getActivity()))
                    new GetSavedOrderDetails(false).execute();
                else
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);

        tOrders = (TextView) savedOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_pending_orders_present);
        tOrders.setVisibility(View.GONE);



        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }




    public void clickListener(final int position) {
        RecyclerView mRecyclerView;

        RecyclerView.LayoutManager mLayoutManager;

         final int[] flag = {0};

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.orders_details_list))
                .setGravity(Gravity.CENTER)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                    }
                })
                .setExpanded(false)
                .setCancelable(true)
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialog) {
                        if(flag[0]==1)
                        deliveryTime(position);
                    }
                })
                .create();



        Button processOrder, close, edit, delete;
        Space space;
        TextView txtComments;

        mRecyclerView = (RecyclerView) dialog.findViewById(R.id.orderdetailsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        RecyclerView.Adapter orderAdapter = new OrderDetailsRecyclerViewAdapter(Master.savedOrderArrayList.get(position).getPlacedOrderItemsList(position));
        mRecyclerView.setAdapter(orderAdapter);
        processOrder = (Button) dialog.findViewById(R.id.processOrderBtn);
        space = (Space) dialog.findViewById(R.id.txtspace);
        txtComments = (TextView) dialog.findViewById(R.id.comments);

        close = (Button) dialog.findViewById(R.id.closeBtn);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 flag[0] = 0;
                dialog.dismiss();
            }
        });

        processOrder.setVisibility(View.VISIBLE);
        space.setVisibility(View.VISIBLE);
        txtComments.setVisibility(View.GONE);
        processOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag[0] =1;
                dialog.dismiss();
            }

        });

        edit = (Button) dialog.findViewById(R.id.editOrderBtn);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditOrderActivity.class);
                intent.putExtra("selectedOrder", Master.savedOrderArrayList.get(position));
                intent.putExtra("position", position);
                intent.putExtra("stockEnabledStatus", stockEnabledStatus);
                intent.putExtra("orderId", Master.savedOrderArrayList.get(position).getOrderId());
                intent.putExtra("minimumBillAmount", minimumBillAmount);
                startActivity(intent);
                dialog.dismiss();
            }
        });


        delete = (Button) dialog.findViewById(R.id.deleteOrderBtn);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
                    new DeleteOrder(dialog,position).execute("" + Master.savedOrderArrayList.get(position).getOrderId());
                else
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

            }
        });


        dialog.show();

    }





    public void deliveryTime(final int position)
    {
        final int[] flag = {0};

        final DialogPlus dialog1 = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.orders_delivery_time))
                .setGravity(Gravity.CENTER)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(false)
                .setCancelable(true)
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialog) {

                        if(flag[0] ==1) {
                            clickListener(position);
                        }
                    }
                })
                .create();



        Button back, cont;


        cont = (Button) dialog1.findViewById(R.id.continueBtn);

        back = (Button) dialog1.findViewById(R.id.backBtn);

        wheelView = (WheelView) dialog1.findViewById(R.id.wheelview_days);
        wheelView.setWheelAdapter(new WheelViewAdapter(getActivity()));
        wheelView.setWheelSize(5);
        wheelView.setSkin(WheelView.Skin.Holo);
        wheelView.setWheelData(createArrays());
        wheelView.setSelection(2);
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.textColor = Color.DKGRAY;
        style.selectedTextColor = Color.GREEN;
        wheelView.setStyle(style);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag[0] =1;
                dialog1.dismiss();
            }
        });

        cont.setVisibility(View.VISIBLE);

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))

                {
                    int no = wheelView.getCurrentPosition();

                    JSONObject jObj = new JSONObject();
                    try {
                        jObj.put("id",Master.savedOrderArrayList.get(position).getOrderId());

                        if(no==0)
                        {
                            jObj.put("date","Not Applicable");

                        }
                        else
                        {
                            no--;

                            Date date = new Date();
                            Timestamp ts = new Timestamp(date.getTime()); //Date could be faked
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(ts);
                            cal.add(Calendar.DAY_OF_WEEK, no);
                            ts.setTime(cal.getTime().getTime());
                            jObj.put("date", String.valueOf(ts));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                     new ChangeSavedToProcessedOrder(dialog1, position).execute(jObj);
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                }

            }
        });


        dialog1.show();

    }




    private ArrayList<String> createArrays() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Not Applicable");
        for (int i = 0; i <= 10; i++) {
            list.add(i+" Days");
        }
        return list;
    }











    //-----------------------------------------Class for change saved to processed orders-------------------------------


    public class ChangeSavedToProcessedOrder extends AsyncTask<JSONObject, String, String> {
        ProgressDialog pd;
        final DialogPlus d;
        final int pos;

        public ChangeSavedToProcessedOrder(DialogPlus dialogPlus, int position) {
            d = dialogPlus;
            pos = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            String changeSavedToProcessedOrderURL = Master.getChangeSavedToProcessedOrderDelDateURL();
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(changeSavedToProcessedOrderURL,params[0] , "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {
            if (pd != null && pd.isShowing())
                pd.dismiss();

            if (PlacedOrderFragment.this.isAdded()) {
                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                } else {
                    try {
                        responseObject = new JSONObject(response);
                        if (responseObject.getString("result").equals("success"))
                            {
                            Toast.makeText(getActivity(), R.string.label_toast_Change_saved_to_processed_order, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString("result").equals("failure") && responseObject.getString("error").equals("order already processed")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_already_processed, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        }else if (responseObject.getString("result").equals("failure") && responseObject.getString("error").equals("order already cancelled")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        }
                        else if(responseObject.getString("result").equals("failure") && responseObject.getString("error").equals("Order has already been deleted")){
                            Toast.makeText(getActivity(), getString(R.string.label_toast_processing_of_deleted_order), Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                            // Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }
    }

    //----------------------------------------------------Getting placed orders---------------------------------------------------
    public class GetSavedOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        final Boolean showProgressDialog;

        public GetSavedOrderDetails(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }


        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            String savedOrderURL = Master.getSavedOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(savedOrderURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog) {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if (PlacedOrderFragment.this.isAdded()) {
                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                } else {
                    try {

                        responseObject = new JSONObject(response);
                        stockEnabledStatus = responseObject.getString("stockManagement");
                        minimumBillAmount = "" + responseObject.getInt("minimumBillOrder");
                        JSONArray jsonArray = responseObject.getJSONArray("orders");
                        if (jsonArray.length() == 0) {
                            mRecyclerView.setVisibility(View.GONE);
                            tOrders.setVisibility(View.VISIBLE);
                        } else {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            tOrders.setVisibility(View.GONE);
                            ++count;

                            Master.savedOrderArrayList = getOrders(jsonArray);

                            mAdapter = new OrderRecyclerViewAdapter(Master.savedOrderArrayList, getActivity(),TAG, false);
                            if (count < 2) {

                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.savedToProcessedKey, PlacedOrderFragment.this));
                                mAdapter.notifyDataSetChanged();
                            } else {

                                mRecyclerView.swapAdapter(mAdapter, true);
                            }
                        }

                        swipeContainer.setRefreshing(false);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }
    }

//----------------------deleting order--------------------------------------------------------------------------------

    public class DeleteOrder extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        final DialogPlus d;
        final int pos;

        public DeleteOrder(DialogPlus dialogPlus,int position) {

            pos = position;
            d= dialogPlus;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject obj = new JSONObject();
            try {
                obj.put("status", "deleted");
                obj.put("comments", "cancelled by admin");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            String deleteOrderURL = Master.getDeleteOrderURL(Integer.valueOf(params[0]));
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(deleteOrderURL, obj, "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());


        }

        protected void onPostExecute(String response) {


            if (pd != null && pd.isShowing())
                pd.dismiss();

            if (PlacedOrderFragment.this.isAdded()) {
                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                } else {
                    try {
                        responseObject = new JSONObject(response);
                        if (responseObject.getString("status").equals("Success")) {
                            Toast.makeText(getActivity(), R.string.label_toast_delete_order, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString("status").equals("error") && responseObject.getString("error").equals("Order has already been deleted by you")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString("status").equals("error") && responseObject.getString("error").equals("Order has already been deleted by the User")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                            //Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                    }
                }


            }

        }

    }
}
