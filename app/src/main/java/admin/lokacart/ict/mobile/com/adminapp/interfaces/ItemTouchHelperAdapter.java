package admin.lokacart.ict.mobile.com.adminapp.interfaces;

/**
 * Created by madhav on 22/5/16.
 */
public interface ItemTouchHelperAdapter {

void onItemMove(int fromPosition, int toPosition);

 void onItemDismiss(int position);
}
