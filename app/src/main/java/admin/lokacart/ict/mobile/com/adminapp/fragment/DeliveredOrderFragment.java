package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.adapter.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.adapter.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.container.SavedOrder;

/**
 * Created by madhav on 10/6/16.
 */



public class DeliveredOrderFragment extends Fragment {

    private View deliveredOrderFragmentView;
    private SwipeRefreshLayout swipeContainer;
    private static ArrayList<SavedOrder> deliveredOrderArrayList;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public static final String TAG = "DeliveredOrderFragment";

    private TextView tOrders;
    private int count=0;
    private JSONObject responseObject;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){



        deliveredOrderFragmentView=inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);

        swipeContainer = (SwipeRefreshLayout) deliveredOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Master.isNetworkAvailable(getActivity()))
                    new GetDeliveredOrderList(false).execute();
                else
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);


        return deliveredOrderFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        deliveredOrderArrayList = new ArrayList<>();
    }






    private ArrayList<SavedOrder> getOrders(JSONArray jsonArray) {

        ArrayList<SavedOrder> orders = new ArrayList<>();
        int size= jsonArray.length();


        if(size!=0)

        {

            int recyclerViewIndex;
            for(recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
            {

                try {

                    SavedOrder order = new SavedOrder((JSONObject) jsonArray.get(recyclerViewIndex), recyclerViewIndex, Master.DELIVEREDORDER);
                    orders.add(order);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }

        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);


        if(Master.isNetworkAvailable(getActivity()))
            new GetDeliveredOrderList(true).execute();
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();


        mRecyclerView = (RecyclerView) deliveredOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        tOrders = (TextView) deliveredOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_delivered_orders_present);
        tOrders.setVisibility(View.GONE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

    }



   /* @Override
    public void onResume() {
        super.onResume();
            mAdapter = new OrderRecyclerViewAdapter(deliveredOrderArrayList, getActivity(), true);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

    }

*/

    public void clickListener(int position){

        new ViewBillAsyncTask(getActivity(),position,deliveredOrderArrayList.get(position).getPayment()).execute("" + deliveredOrderArrayList.get(position).getOrderId());

    }

    //------------------------------------------------API to get the Delivered Order List---------------------------------

    public class GetDeliveredOrderList extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        final Boolean showProgressDialog;

        public GetDeliveredOrderList(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Void... params)
        {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getDeliveredOrderList(AdminDetails.getAbbr()), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response)
        {
            if(showProgressDialog)
            {   if(pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if(DeliveredOrderFragment.this.isAdded()){

                if(response.equals("exception")){
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else{

                    try {
                        responseObject = new JSONObject(response);
                        JSONArray jsonArray = responseObject.getJSONArray("orders");
                        if (jsonArray.length() == 0) {
                            mRecyclerView.setVisibility(View.GONE);
                            tOrders.setVisibility(View.VISIBLE);
                        } else {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            tOrders.setVisibility(View.GONE);
                            ++count;

                            deliveredOrderArrayList = getOrders(jsonArray);

                            mAdapter = new OrderRecyclerViewAdapter(deliveredOrderArrayList, getActivity(),TAG, true);
                            if (count < 2) {

                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.deliveredOrderKey, DeliveredOrderFragment.this));
                                mAdapter.notifyDataSetChanged();
                            } else {

                                mRecyclerView.swapAdapter(mAdapter, true);
                            }
                        }

                        swipeContainer.setRefreshing(false);

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }

                }
            }

        }
    }

    //---------------------------------------------------API to get the view bill --------------------------------------------------------------------

    public class ViewBillAsyncTask extends AsyncTask<String,String,String>
    {
        final Context context;
        ProgressDialog pd;
        final int position;
        final String payment;

        ViewBillAsyncTask(Context context,int position,String payment) {
            this.context = context;
            this.position=position;
            this.payment=payment;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String status;
            HttpURLConnection linkConnection = null;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String emailid = sharedPreferences.getString("emailid","test2@gmail.com");
            String password =  sharedPreferences.getString("password","password");

            try {

                URL linkurl = new URL(Master.getDeliveredOrderViewBillURL(AdminDetails.getAbbr(),params[0]));
                linkConnection = (HttpURLConnection) linkurl.openConnection();
                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
                linkConnection.setDefaultUseCaches(false);
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "text/html");
                linkConnection.setDoInput(true);
                InputStream is ;
                status=String.valueOf(linkConnection.getResponseCode());
                if(status.equals("200"))
                {
                    is=linkConnection.getInputStream();
                }
                else
                {
                    status="exception";
                    return  status;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line ;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                status = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
                status="exception";
                return status;
            }
            finally {
                if (linkConnection != null) {
                    linkConnection.disconnect();
                }
            }
            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(pd != null && pd.isShowing())
                pd.dismiss();


            if(DeliveredOrderFragment.this.isAdded()){
                if (s.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle(R.string.dialog_title_bill);
                    WebView wv = new WebView(context);
                    wv.loadData(s, "text/html", "UTF-8");
                    wv.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                    alert.setView(wv);

                    if(payment.equals("false")) {
                        alert.setNegativeButton(R.string.dialog_paid, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AlertDialog.Builder confirm_alert = new AlertDialog.Builder(context);
                                confirm_alert.setTitle(R.string.dialog_title_are_you_sure_the_order_is_paid);
                                confirm_alert.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ChangePaidStateTask(position, deliveredOrderArrayList.get(position).getOrderId()).execute();
                                    }
                                });
                                confirm_alert.setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                confirm_alert.show();

                            }
                        });
                    }
                    alert.setPositiveButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            }

        }
    }

    public class ChangePaidStateTask extends AsyncTask<String,String,String> {

        ProgressDialog pd;
        final Boolean showProgressDialog;
        final int position;
        final int orderId;

        public ChangePaidStateTask(int position, int orderId) {
            this.showProgressDialog = true;
            this.position=position;
            this.orderId=orderId;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getChangePaidState(orderId), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog) {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if (DeliveredOrderFragment.this.isAdded()) {

                if(response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));

                }
                else
                {

                    try {
                        responseObject = new JSONObject(response);
                        response = responseObject.getString("response");
                        switch (response) {
                            case "failure":
                                Toast.makeText(getActivity(), R.string.dialog_order_already_paid, Toast.LENGTH_SHORT).show();
                                break;
                            case "Success":
                                deliveredOrderArrayList.get(position).setPayment();
                                mAdapter.notifyItemChanged(position);
                                break;
                            default:

                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }



                }

            }
        }
    }

}
