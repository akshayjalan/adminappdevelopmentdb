
package admin.lokacart.ict.mobile.com.adminapp.fragment;

/**
 * Created by Vishesh on 19-01-2016.
 */
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;

import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.interfaces.NavigationItemListener;

public class DashboardFragment extends Fragment implements View.OnClickListener{

    private TextView tPlacedOrders;
    private TextView tProcessedOrders;
    private TextView tCancelledOrders;
    private TextView tTotalUsers;
    private TextView tDeliveredOrders;
    private TextView tPaidDeliveredOrders;
    private TextView tUnpaidDeiveredOrders;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private NavigationItemListener callback;
    private View dashboardFragmentView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        DashboardActivity.resetBackPress();

        if(DashboardFragment.this.isAdded() && getActivity()!=null)
        ((DashboardActivity)getActivity()).updateStatusBarColor();

        dashboardFragmentView = inflater.inflate(R.layout.fragment_dashboard_new2, container, false);
        getActivity().setTitle(R.string.title_dashboard);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = sharedPreferences.edit();
        callback = (NavigationItemListener) getActivity();
        callback.itemSelected(Master.getDashboardTAG());
        return dashboardFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        CardView cardViewMembers = (CardView) dashboardFragmentView.findViewById(R.id.cardViewMembers);
        cardViewMembers.setOnClickListener(this);

        CardView cardViewPlacedOrders = (CardView) dashboardFragmentView.findViewById(R.id.cardViewPlacedOrders);
        cardViewPlacedOrders.setOnClickListener(this);

        CardView cardViewProcessedOrders = (CardView) dashboardFragmentView.findViewById(R.id.cardViewProcessedOrders);
        cardViewProcessedOrders.setOnClickListener(this);

        CardView cardViewDeliveredOrders = (CardView) dashboardFragmentView.findViewById(R.id.cardViewDeliveredOrders);
        cardViewDeliveredOrders.setOnClickListener(this);

        CardView cardViewCancelledOrders = (CardView) dashboardFragmentView.findViewById(R.id.cardViewCancelledOrders);
        cardViewCancelledOrders.setOnClickListener(this);


        tPlacedOrders = (TextView) dashboardFragmentView.findViewById(R.id.tPlacedOrders);

        tProcessedOrders = (TextView) dashboardFragmentView.findViewById(R.id.tProcessedOrders);

        tCancelledOrders = (TextView) dashboardFragmentView.findViewById(R.id.tCancelledOrders);

        tTotalUsers = (TextView) dashboardFragmentView.findViewById(R.id.tTotalUsers);
        tPaidDeliveredOrders=(TextView) dashboardFragmentView.findViewById(R.id.tPaidDeliveredOrders);

        tUnpaidDeiveredOrders=(TextView) dashboardFragmentView.findViewById(R.id.tUnpaidDeliveredOrders);

        if(Master.isNetworkAvailable(getActivity()))
            new GetDashboardDetailsTask().execute();
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View v) {

        OrderFragment orderFragment = new OrderFragment();
        ExistingUserFragment memberFragment = new ExistingUserFragment();
        Bundle bundle = new Bundle();
        DashboardActivity.onBackDashboard = true;

        switch (v.getId())
        {
            case R.id.cardViewPlacedOrders:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new OrderFragment()).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.cardViewProcessedOrders:
                bundle.putString("to", "processed");
                orderFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, orderFragment).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.cardViewDeliveredOrders:
                bundle.putString("to","delivered");
                orderFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, orderFragment).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.cardViewCancelledOrders:
                bundle.putString("to", "cancelled");
                orderFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, orderFragment).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.cardViewMembers:
                bundle.putString("to", "existing");
                memberFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, memberFragment, DashboardActivity.MEMBER_TAG).commit();
                callback.itemSelected(Master.getMembersTAG());
                break;

        }
    }

    public void update(int params[])
    {
        if(params[0] != sharedPreferences.getInt("pendingOrders", 0))
        {
            tPlacedOrders.setText(params[0] + "");
            editor.putInt("pendingOrders", params[0]);
        }
        if(params[1] != sharedPreferences.getInt("processedOrders", 0))
        {
            tProcessedOrders.setText(params[1] + "");
            editor.putInt("processedOrders", params[1]);
        }
        if(params[2] != sharedPreferences.getInt("cancelledOrders", 0))
        {
            tCancelledOrders.setText(params[2] + "");
            editor.putInt("cancelledOrders", params[2]);
        }
        if(params[3] != sharedPreferences.getInt("totalUsers", 0))
        {
            tTotalUsers.setText(params[3] + "");
            editor.putInt("totalUsers", params[3]);
        }

        if(params[6] != sharedPreferences.getInt("paidOrders", 0))
        {
            tPaidDeliveredOrders.setText(params[6] + "");
            editor.putInt("paidOrders", params[6]);
        }

        if(params[7] != sharedPreferences.getInt("unpaidOrders", 0))
        {
            tUnpaidDeiveredOrders.setText(params[7] + "");
            editor.putInt("unpaidOrders", params[7]);
        }

        if(params[8] != sharedPreferences.getInt("deliveredOrders", 0))
        {
            tDeliveredOrders.setText(params[8] + "");
            editor.putInt("deliveredOrders", params[8]);
        }
        editor.commit();
    }


    public class GetDashboardDetailsTask extends AsyncTask<String, String, String>
    {

        ProgressDialog pd;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getDashboardDetailsURL()+ AdminDetails.getAbbr(), null, "GET", true,AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String message)
        {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(DashboardFragment.this.isAdded())
            {
                if(message.equals("exception"))
                {
                    try
                    {
                        tPlacedOrders.setText(sharedPreferences.getInt("pendingOrders", 0));
                        tProcessedOrders.setText(sharedPreferences.getInt("processedOrders", 0));
                        tCancelledOrders.setText(sharedPreferences.getInt("cancelledOrders", 0));
                        tTotalUsers.setText(sharedPreferences.getInt("totalUsers", 0));
                        tPaidDeliveredOrders.setText(sharedPreferences.getInt("paidOrders", 0));
                        tUnpaidDeiveredOrders.setText(sharedPreferences.getInt("unpaidOrders", 0));

                    }
                    catch (Exception e)
                    {
                       /* if(!Master.isNetworkAvailable(getActivity())) {
                            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getActivity(),R.string.label_toast_something_went_worng,Toast.LENGTH_SHORT).show();
                        }*/
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
                else
                {
                    try
                    {

                        jsonObject = new JSONObject(message);
                        int pendingOrders = jsonObject.getInt("saved");
                        int processedOrders = jsonObject.getInt("processed");
                        int cancelledOrders = jsonObject.getInt("cancelled");
                        int deliveredOrders = jsonObject.getInt("delivered");
                        int totalUsers = jsonObject.getInt("totalUsers");
                        int newUsersToday = jsonObject.getInt("newUsersToday");
                        int pendingRequests = jsonObject.getInt("pendingUsers");
                        int paidDeliveredOrders = jsonObject.getInt("paid");
                        int unpaidDeliveredOrders = jsonObject.getInt("unpaid");

                        tPlacedOrders.setText(pendingOrders + "");
                        tProcessedOrders.setText(processedOrders + "");
                        tCancelledOrders.setText(cancelledOrders + "");
                        tTotalUsers.setText(totalUsers + "");
                        tPaidDeliveredOrders.setText(paidDeliveredOrders + "");
                        tUnpaidDeiveredOrders.setText(unpaidDeliveredOrders + "");



                        editor.putInt("pendingOrders", pendingOrders);
                        editor.putInt("processedOrders", processedOrders);
                        editor.putInt("cancelledOrders", cancelledOrders);
                        editor.putInt("totalUsers", totalUsers);
                        editor.putInt("newUsersToday", newUsersToday);
                        editor.putInt("pendingRequests", pendingRequests);
                        editor.putInt("deliveredOrders", deliveredOrders);
                        editor.putInt("paidOrders", paidDeliveredOrders);
                        editor.putInt("unpaidOrders", unpaidDeliveredOrders);
                        editor.commit();
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        }
    }

}