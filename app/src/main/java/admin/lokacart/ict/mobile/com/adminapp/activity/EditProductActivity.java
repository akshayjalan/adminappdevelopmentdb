package admin.lokacart.ict.mobile.com.adminapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Random;

import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.LokacartAdminApplication;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.util.UploadImageDisplay;

public class EditProductActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText eProductName, eProductQuantity, eProductPrice,eProductDescription;
    private Button bCancel;
    private int position;
    private int recyclerViewIndex;
    private final int REQUEST_GALLERY = 2;
    private final int REQUEST_CAMERA = 1;
    private final int REQUEST_AUDIO = 3;
    private static MediaRecorder mediaRecorder;
    private static MediaPlayer mediaPlayer;
    private static String recordedAudioFilePath;
    private static String selectedAudioFilePath;

    private String tempImage;
    private String fromWhere;
    private String productName;
    private String productId;
    private Random randomno;
    private TextView tDescCharRemaining;
    private TextView tProductNameCharRemaining;
    private Button bPlay;
    private Button bStop;
    private Button bRecord;
    private Button bUpload;
    private boolean isRecording = false, isPlaying = false, isRecorded = false;
    private static final String TAG = "EditProductActivity";
    public static final String DOT_SEPARATOR = ".";


    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Master.getAdminData(getApplicationContext());
        setContentView(R.layout.activity_edit_product);
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        randomno = new Random();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        try
        {

            position = getIntent().getIntExtra("position", 0);
            recyclerViewIndex = getIntent().getIntExtra("size", 0);
            fromWhere=getIntent().getStringExtra("fromWhere");
            productName=getIntent().getStringExtra("product_name");
            String productDescription = getIntent().getStringExtra("description");
            Double productUnitPrice = getIntent().getDoubleExtra("unitPrice", 0);
            int stockQuantity = getIntent().getIntExtra("quantity", 0);
            productId=getIntent().getStringExtra("product_id");


            eProductName = (EditText) findViewById(R.id.eProductName);
            eProductPrice = (EditText) findViewById(R.id.eProductPrice);
            eProductQuantity = (EditText) findViewById(R.id.eProductQuantity);
            eProductDescription=(EditText) findViewById(R.id.eProductDescription);
            tDescCharRemaining = (TextView) findViewById(R.id.tDescCharRemaining);
            tProductNameCharRemaining = (TextView)findViewById(R.id.tProductNameCharRemaining);

            if(fromWhere != null && fromWhere.equals("fromDashboardActivity")){
                eProductName.setText(productName);
                eProductPrice.setText(productUnitPrice.toString());
                eProductQuantity.setText(stockQuantity + "");
                eProductDescription.setText(productDescription);

            }else {
                eProductName.setText(Master.productList.get(position));
                eProductPrice.setText(Master.priceList.get(position));
                eProductQuantity.setText(Master.quantityList.get(position));
                eProductDescription.setText(Master.descList.get(position));
                productName = Master.productList.get(position);
            }
            setTitle(productName);
            tDescCharRemaining.setText((500 - eProductDescription.length()) + " " + getString(R.string.textview_characters_left));
            tProductNameCharRemaining.setText((50 - eProductName.length()) + " " + getString(R.string.textview_characters_left));


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        eProductDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != -1)
                    tDescCharRemaining.setText((500 - s.length()) + " " + getString(R.string.textview_characters_left));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        eProductName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != -1)
                    tProductNameCharRemaining.setText((50 - s.length()) + " " + getString(R.string.textview_characters_left));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        Button bProductSave = (Button) findViewById(R.id.bProductSave);
        bProductSave.setOnClickListener(this);

        Button bProductCancel = (Button) findViewById(R.id.bProductCancel);
        bProductCancel.setOnClickListener(this);

        Button bProductUploadImage = (Button) findViewById(R.id.bProductUploadImage);
        bProductUploadImage.setOnClickListener(this);

        Button bProductUploadAudio = (Button) findViewById(R.id.bProductUploadAudio);
        bProductUploadAudio.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bProductSave:
                save();
                break;

            case R.id.bProductCancel:
                finish();
                break;

            case R.id.bProductUploadImage:
                uploadImage();
                break;

            case R.id.bProductUploadAudio:
                uploadAudio();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        Master.getAdminData(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mediaRecorder != null){
            mediaRecorder.stop();
        }
    }

    private void uploadImage()
    {
        final CharSequence[] options = { getString(R.string.upload_image_take_photo), getString(R.string.upload_image_choose_from_gallery),getString(R.string.label_button_cancel) };
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProductActivity.this);
        builder.setTitle(R.string.upload_image_title);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    tempImage = "temp" + randomno.nextInt(1000000) + ".jpg";
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), tempImage);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/jpg");
                    startActivityForResult(intent, REQUEST_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void uploadAudio() {



        final CharSequence[] options = { getString(R.string.upload_audio_record_audio), getString(R.string.upload_audio_choose_from_gallery), getString(R.string.upload_audio_cancel) };
        final AlertDialog.Builder builder = new AlertDialog.Builder(EditProductActivity.this);
        builder.setTitle(R.string.upload_audio_title);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (options[which].equals("Record audio")) {
                    record();
                } else if (options[which].equals("Choose from memory")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("audio/*");
                    startActivityForResult(intent, REQUEST_AUDIO);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void record() {

        final String  productId;

        productId = Master.productIdList.get(position);

        final DialogPlus dialogPlus = Master.dialogPlus(new ViewHolder(R.layout.audio_box), EditProductActivity.this);

        bPlay = (Button) dialogPlus.findViewById(R.id.bPlay);
        bRecord = (Button) dialogPlus.findViewById(R.id.bRecord);
        bStop = (Button) dialogPlus.findViewById(R.id.bStop);
        bUpload = (Button) dialogPlus.findViewById(R.id.bUpload);
        bCancel = (Button) dialogPlus.findViewById(R.id.bCancel);

        if (!hasMicrophone())
        {
            bStop.setEnabled(false);
            bPlay.setEnabled(false);
            bRecord.setEnabled(false);

            Toast.makeText(EditProductActivity.this,
                    R.string.label_toast_microphone_is_not_functioning_properly, Toast.LENGTH_LONG).show();
        }
        else {
            bPlay.setEnabled(false);
            bStop.setEnabled(false);

            String tempAudio = "/myaudio" + randomno.nextInt(1000000) + ".mp3";
            recordedAudioFilePath = Environment.getExternalStorageDirectory() + tempAudio;

            TextView audio = (TextView) findViewById(R.id.tAudioLink);
            audio.setText(recordedAudioFilePath);

            bRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recordAudio(recordedAudioFilePath);
                }
            });

            bPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        playAudio(recordedAudioFilePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            bStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopAudio();
                }
            });

            bUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isRecorded)
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_please_record_an_audio_to_upload, Toast.LENGTH_SHORT).show();
                    }
                    else if(!Master.isNetworkAvailable(EditProductActivity.this))
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        new UploadAudioTask(recordedAudioFilePath,productId, dialogPlus).execute();
                    }
                    if(isPlaying)
                        stopAudio();
                }
            });

            bCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogPlus.dismiss();
                    isRecorded = false;
                    try {
                        file.delete();
                    } catch (Exception ignored) {

                    }
                }
            });
        }
    }


    private void recordAudio(String filepath) {
        isRecording = true;
        bStop.setEnabled(true);
        bPlay.setEnabled(false);
        bRecord.setEnabled(false);
        bUpload.setEnabled(false);

        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setOutputFile(filepath);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mediaRecorder.start();

        mediaRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
                Toast.makeText(EditProductActivity.this, R.string.label_toast_the_recorder_couldnt_be_started_please_try_again, Toast.LENGTH_LONG).show();
            }
        });
        isRecorded = true;
    }

    private void stopAudio()
    {
        isPlaying = false;
        bStop.setEnabled(false);
        bPlay.setEnabled(true);
        bUpload.setEnabled(true);
        if (isRecording)
        {
            bRecord.setEnabled(false);
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
            isRecording = false;
        }
        else
        {
            mediaPlayer.release();
            mediaPlayer = null;
            bRecord.setEnabled(true);
        }
    }

    private void playAudio(String filepath) throws IOException
    {
        isPlaying = true;
        bPlay.setEnabled(false);
        bRecord.setEnabled(false);
        bStop.setEnabled(true);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(filepath);
        mediaPlayer.prepare();
        mediaPlayer.start();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                bPlay.setEnabled(true);
                bRecord.setEnabled(true);
                bStop.setEnabled(false);
            }
        });

        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(EditProductActivity.this, R.string.label_toast_the_player_couldnt_be_started_please_try_again, Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    private boolean hasMicrophone() {
        PackageManager pmanager = this.getPackageManager();
        return pmanager.hasSystemFeature(
                PackageManager.FEATURE_MICROPHONE);
    }



    public  String getFileExtension(String name) {



        int extIndex = name.lastIndexOf(EditProductActivity.DOT_SEPARATOR);

        if (extIndex == -1) {
            return "";
        } else {
            return name.substring(extIndex + 1);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }

                try {
                        final String  productId;

                        productId = Master.productIdList.get(position);

                        String filePath = f.getAbsolutePath();
                        Intent intent = new Intent(getApplicationContext(),UploadImageDisplay.class);
                        intent.putExtra("imgpath",filePath);
                        intent.putExtra("productname",productName);
                        intent.putExtra("TAG", TAG);
                        intent.putExtra("id",productId);
                        startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            else if (requestCode == REQUEST_GALLERY)
            {
                Uri selectedImageURI = data.getData();
                if(selectedImageURI != null) {
                    try {
                        String selectedImageFilePath = Master.getPath(EditProductActivity.this, selectedImageURI);

                        File f = new File(selectedImageFilePath);

                        String ext = getFileExtension(f.getName());

                        if(ext.equalsIgnoreCase("jpg")||ext.equalsIgnoreCase("jpeg"))
                        {
                            final String  productId;

                            productId = Master.productIdList.get(position);
                            Intent intent = new Intent(getApplicationContext(),UploadImageDisplay.class);
                            intent.putExtra("imgpath", selectedImageFilePath);
                            intent.putExtra("productname",productName);
                            intent.putExtra("TAG", TAG);
                            intent.putExtra("id",productId);

                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(EditProductActivity.this, R.string.label_toast_upload_jpg_file, Toast.LENGTH_LONG).show();

                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_unable_to_get_the_file_path, Toast.LENGTH_LONG).show();
                    }
                }
            }
            else if(requestCode == REQUEST_AUDIO)
            {

                try {
                    selectedAudioFilePath = Master.getPath(EditProductActivity.this, data.getData());

                    final File file = new File(selectedAudioFilePath);

                    String ext = getFileExtension(file.getName());

                    if(ext.equalsIgnoreCase("mp3"))
                    {
                        final DialogPlus dialogPlus = Master.dialogPlus(new ViewHolder(R.layout.audio_box), EditProductActivity.this);
                        bPlay = (Button) dialogPlus.findViewById(R.id.bPlay);
                        bRecord = (Button) dialogPlus.findViewById(R.id.bRecord);
                        bStop = (Button) dialogPlus.findViewById(R.id.bStop);
                        bStop.setEnabled(false);
                        bUpload = (Button) dialogPlus.findViewById(R.id.bUpload);
                        bCancel = (Button) dialogPlus.findViewById(R.id.bCancel);

                        bRecord.setVisibility(View.GONE);

                        TextView audio = (TextView) dialogPlus.findViewById(R.id.tAudioLink);
                        audio.setText(selectedAudioFilePath);


                        MediaPlayer mp = new MediaPlayer();
                        FileInputStream fs;
                        FileDescriptor fd;
                        try {
                            fs = new FileInputStream(file);
                            fd = fs.getFD();
                            mp.setDataSource(fd);
                            mp.prepare();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        mp.release();


                        bPlay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    playAudio(selectedAudioFilePath);
                                } catch (IOException e) {
                                    Toast.makeText(EditProductActivity.this, R.string.label_toast_cannot_play_audio, Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        bStop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stopAudio();
                            }
                        });

                        bUpload.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!Master.isNetworkAvailable(EditProductActivity.this))
                                {
                                    Toast.makeText(EditProductActivity.this, R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    final String  productId;

                                    productId = Master.productIdList.get(position);
                                    new UploadAudioTask(selectedAudioFilePath,productId, dialogPlus).execute();
                                }
                                if (isPlaying)
                                    stopAudio();
                            }
                        });

                        bCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogPlus.dismiss();
                            }
                        });

                    }
                    else
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_upload_mp3_file, Toast.LENGTH_LONG).show();

                    }



                }
                catch (Exception e)
                {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_unable_to_get_the_file_path, Toast.LENGTH_LONG).show();
                }
            }
        }
    }



    private void save()
    {
        String productName, productId;

        if(fromWhere != null && fromWhere.equals("fromDashboardActivity")){
            productName = this.productName ;
            productId=this.productId;


       }else{

            productId = Master.productIdList.get(position);

            productName = Master.productList.get(position);

       }

        String enteredName, enteredPrice, enteredQuantity,enteredDescription;
        enteredName = eProductName.getText().toString().trim();
        enteredPrice = eProductPrice.getText().toString().trim();
        enteredQuantity = eProductQuantity.getText().toString().trim();
        enteredDescription=eProductDescription.getText().toString().trim();

        if(enteredName.equals(""))
        {
            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_a_product_name, Toast.LENGTH_SHORT).show();
        }
        else if(enteredPrice.equals(""))
        {
            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_price,Toast.LENGTH_SHORT).show();
        }
        else if(enteredQuantity.equals(""))
        {
            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_quantity,Toast.LENGTH_SHORT).show();
        }
        else
        {



            if (!enteredPrice.equals("")) {

                try {

                    double price = Double.parseDouble(enteredPrice);
                    if (price == 0.0) {
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_price_greater_than_zero, Toast.LENGTH_SHORT).show();

                    }
                    else
                    {

                        boolean flag = false;

                        DecimalFormat decimalFormat=new DecimalFormat("#.00");
                        enteredPrice=decimalFormat.format(price);

                        for(int i = 0; i < recyclerViewIndex; ++i)
                        {
                            if(i == position) // Skip checking the same product
                                continue;

                            if(Master.productList.get(i).equals(enteredName))
                            {
                                flag = true;
                                break;
                            }
                        }
                        if(!flag)
                        {
                            JSONObject jsonObject = new JSONObject();
                            try
                            {
                                int id = Integer.parseInt(productId);

                                jsonObject.put("id",id);
                                jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                jsonObject.put("name", productName);
                                jsonObject.put("newname", enteredName);
                                jsonObject.put("qty", enteredQuantity);
                                jsonObject.put("rate", enteredPrice);
                                jsonObject.put("newdesc",enteredDescription);
                            }
                            catch(JSONException e)
                            {
                                e.printStackTrace();
                              //  Toast.makeText(EditProductActivity.this, R.string.label_toast_data_send_failed, Toast.LENGTH_SHORT).show();
                            }
                            if(Master.isNetworkAvailable(EditProductActivity.this))
                            {


                                 new EditProductTask(EditProductActivity.this, position, enteredName, enteredPrice, enteredQuantity,enteredDescription,productId).execute(jsonObject);
                            }
                            else
                            {
                                Toast.makeText(EditProductActivity.this, R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                        }



                    }
                } catch (Exception e) {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_please_enter_valid_price, Toast.LENGTH_SHORT).show();

                }
            }

        }
    }

    //---------------------EditProduct Asynctask---------------------------------------------------------

    //-------------------------------------for editing product-----------------------------------
    public class EditProductTask extends AsyncTask<JSONObject, String, String>
    {
        final Context context;
        final String productName;
        final String productPrice;
        final String productQuantity;
        final String productDescription;
        final String productId;
        ProgressDialog pd;
        final int position;

        EditProductTask(Context context, int position, String productName, String productPrice, String productQuantity ,String productDescription,String productId )
        {
            this.context = context;
            this.productName = productName;
            this.productPrice = productPrice;
            this.productQuantity = productQuantity;
            this.productDescription=productDescription;
            this.position = position;
            this.productId=productId;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getEditProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());

        }

        @Override
        protected void onPostExecute(String message) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(message.equals("exception"))

            {
                Toast.makeText(EditProductActivity.this, R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

            }
             else
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(message);
                    if(jsonObject.getString("edit").equals("success"))
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_toast_Product_updated_successfully, Toast.LENGTH_SHORT).show();
                        if(fromWhere==null) {
                            Master.productList.set(position, productName);
                            Master.priceList.set(position, productPrice);
                            Master.quantityList.set(position, productQuantity);
                            Master.descList.set(position, productDescription);
                        }

                        updateProductSearchList(productId,productName,productDescription,productPrice,productQuantity);
                    }
                    else
                    {
                        Toast.makeText(EditProductActivity.this, R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                        //Master.alertDialog(EditProductActivity.this, getString(R.string.label_toast_something_went_worng), getString(R.string.label_alertdialog_ok));
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(EditProductActivity.this, R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                }
            }


        }
    }



    private int updateProductSearchList(String id, String name, String description, String price, String quantity)
    {

        for(int i = 0; i < Master.productTypeSearchList.size(); ++i)
        {
            for(int j = 0; j < Master.productTypeSearchList.get(i).productItems.size(); ++j)
            {
                if(Master.productTypeSearchList.get(i).productItems.get(j).getID().equals(id))
                {
                    Master.productTypeSearchList.get(i).productItems.get(j).setName(name);
                    Master.productTypeSearchList.get(i).productItems.get(j).setDescription(description);
                    Master.productTypeSearchList.get(i).productItems.get(j).setUnitPrice(Double.parseDouble(price));
                    Master.productTypeSearchList.get(i).productItems.get(j).setStockQuantity(Integer.parseInt(quantity));

                    return j;
                }
            }
        }
        return -1;
    }




    //---------------------End of EditProduct Asynctask-------------------------------------------------




    //------------------------------- Asynctask to upload Audio ----------------------------------------------
    class UploadAudioTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        final String path;
        final DialogPlus dialog;
        final String id;

        UploadAudioTask(String path, String id,DialogPlus dialogPlus)
        {
            this.path = path;
            dialog = dialogPlus;
            this.id=id;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(EditProductActivity.this);
            pd.setMessage(getString(R.string.pd_uploading_file));
            pd.setCancelable(false);
            pd.show();

            file = new File(path);

        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            response = Master.okhttpUpload(file, Master.getUploadAudioURL(id),
                    AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if(pd != null && pd.isShowing())
             pd.dismiss();
            try
            {
                JSONObject responseObject = new JSONObject(s);
                if(responseObject.get("response").toString().equals("Audio upload successful"))
                {
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_audio_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else if(responseObject.get("response").toString().equals("timeout")){
                    Toast.makeText(EditProductActivity.this, R.string.label_toast_timeout_upload, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else
                {

                    Toast.makeText(EditProductActivity.this, R.string.label_toast_audio_upload_failed, Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(EditProductActivity.this, R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                //Toast.makeText(EditProductActivity.this,R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();

            }
        }
    }
//---------------------End of Upload Audio Asynctask-------------------------------------------------
}
