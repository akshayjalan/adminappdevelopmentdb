package admin.lokacart.ict.mobile.com.adminapp;
import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

/**
 * Created by Swapneel on 04/08/16.
 */

public class LokacartAdminApplication extends Application {

    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    private synchronized Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    // The following line should be changed to include the correct property id.
    private static final String PROPERTY_ID = "UA-81945069-2";

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     *
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        // --Commented out by Inspection (28/11/16 11:27 AM):ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    private final HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    private synchronized Tracker getTracker() {
        if (!mTrackers.containsKey(TrackerName.APP_TRACKER)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = analytics.newTracker(PROPERTY_ID);
            mTrackers.put(TrackerName.APP_TRACKER, t);

        }
        return mTrackers.get(TrackerName.APP_TRACKER);
    }

// --Commented out by Inspection START (28/11/16 11:27 AM):
//    synchronized public void AnalyticsEvent(String categoryId, String actionId) {
//
//        Tracker t = getTracker(
//        );
//        // Build and send an Event.
//        t.send(new HitBuilders.EventBuilder()
//                .setCategory(categoryId)
//                .setAction(actionId)
//                .build());
//    }
// --Commented out by Inspection STOP (28/11/16 11:27 AM)

    synchronized public void AnalyticsActivity(String TAG) {
        // Obtain the shared Tracker instance.
        Tracker mTracker = getDefaultTracker();

        //Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Screen~" + TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }
}

